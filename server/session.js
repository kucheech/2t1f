
const uuid = require('uuid/v4');

global.sessions = [];

function exists(id) {
  return global.sessions.includes(id);
}

function add(id) {
  global.sessions.push(id);
}

function remove(id) {
  global.sessions.filter(item => item !== id);
}

function create() {
  let id = null;
  do {
    id = uuid().toString().substring(0, 4).toUpperCase();
  } while (exists(id));
  add(id);
  return id;
}

module.exports = {
  create,
  remove,
};
