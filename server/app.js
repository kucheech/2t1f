const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const qrcode = require('qrcode');

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const session = require('./session');
const { CLIENT } = require('./config');

const CLIENT_FOLDER = path.join(__dirname, CLIENT);
app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const sockets = new Set();

const socket = io.on('connection', (s) => {
  sockets.add(s);
  // console.log(`Number of sockets: ${sockets.size}`);
  // s.on('client', (data) => {
  //   console.log(`client data: ${data}`);
  // });
  // s.on('server', (data) => {
  //   console.log(`socket data: ${data}`);
  // });

  s.on('relay', (data) => {
    socket.emit(data.sid, data);
  });

  s.on('disconnect', () => {
    // console.log(`Deleting socket: ${s.id}`);
    sockets.delete(s);
    // console.log(`Remaining sockets: ${sockets.size}`);
  });
});

app.get('/session/create', (req, res) => {
  const id = session.create();
  res.status(200).json({ id });
});

app.post('/session/start', (req, res) => {
  const { sid } = req.body;
  socket.emit(sid, { command: 'start-game' });
  res.status(200).json({ sid });
});

app.delete('/session/:id', (req, res) => {
  const { id } = req.params;
  session.remove(id);
  res.sendStatus(200);
});

app.get('/qrcode/:id', (req, res) => {
  const { id } = req.params;
  const link = `${req.protocol}://${req.get('host')}/entry/add/${id}`;
  const opts = {
    width: 250,
  };
  qrcode.toDataURL(link, opts)
    .then((dataUrl) => {
      res.status(200).send({ dataUrl });
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});

app.post('/session/entry', (req, res) => {
  const entry = {
    sid: req.body.sid,
    name: req.body.name,
    t1: req.body.t1,
    t2: req.body.t2,
    f: req.body.f,
  };

  socket.emit(entry.sid, { command: 'add-entry', entry });
  res.status(200).json(entry);

  // todo: to check for duplicate entry
});

// catch all
app.use((req, res) => {
  res.sendFile(path.join(CLIENT_FOLDER, 'index.html'));
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
  console.log(`App started at port ${PORT}`);
});
