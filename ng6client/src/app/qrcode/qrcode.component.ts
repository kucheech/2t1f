import { Component, OnInit, Input } from '@angular/core';
import { Session } from '../session';
import { SessionService } from '../session.service';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrls: ['./qrcode.component.css']
})
export class QRCodeComponent implements OnInit {
  private _session: Session = null;

  @Input()
  set session(session: Session) {
    if (!!session) {
      this._session = session;
      this.getQRCode();
    }
  }
  get session() { return this._session; }

  dataUrl: string = null;

  constructor(private sessionService: SessionService) { }

  ngOnInit() {
  }

  getQRCode() {
    this.sessionService.getQRCode(this.session)
      .subscribe(qrcode => {
        this.dataUrl = qrcode.dataUrl;
      });
  }

}
