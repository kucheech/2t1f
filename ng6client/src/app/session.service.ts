import { Injectable } from '@angular/core';
import { Session } from './session';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Entry } from './entry';
import { QRCode } from './qrcode';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private http: HttpClient) { }

  createSession(): Observable<Session> {
    return this.http.get<Session>('/session/create');
  }

  startGame(sid) {
    return this.http.post('/session/start', { sid });
  }

  submitEntry(entry: Entry) {
    this.http.post('/session/entry', entry).subscribe();
  }

  getQRCode(session: Session): Observable<QRCode> {
    return this.http.get<QRCode>('/qrcode/' + session.id);
  }
}
