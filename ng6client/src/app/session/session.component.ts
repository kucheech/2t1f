import { Component, OnInit } from '@angular/core';
import { Session } from '../session';
import { SessionService } from '../session.service';
import { Title } from '@angular/platform-browser';
import { SocketService } from '../socket.service';
import { Entry } from '../entry';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css']
})
export class SessionComponent implements OnInit {

  session: Session = null;
  announcements: string[] = [];
  entries: Entry[] = [];
  gameStarted = false;

  constructor(private sessionService: SessionService,
    private titleService: Title,
    private socketService: SocketService,
    private dataService: DataService,
    private router: Router,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    this.dataService.clearData();
    this.createSession();
  }

  createSession() {
    this.sessionService.createSession()
      .subscribe(session => {
        this.session = session;
        this.dataService.setSessionId(session.id);
        this.startSocket();
        this.setTitle();
        this.spinner.hide();
      });
  }

  startSocket() {
    this.socketService.onEvent(this.session.id)
      .subscribe(msg => this.processMsg(msg));
  }

  processMsg(msg) {
    if (msg.command && msg.command === 'add-entry' && msg.entry) {
      this.entries.push(msg.entry);

      if (this.gameStarted) {
        this.dataService.setEntries(this.entries);
      } else {
        this.announcements.unshift(msg.entry.name + ' has just added his/her entry');
      }
    }
  }

  setTitle() {
    this.titleService.setTitle('Session: ' + this.session.id);
  }

  startGame() {
    this.spinner.show();
    this.dataService.setEntries(this.entries);
    this.gameStarted = true;
    this.router.navigateByUrl('/entries');
  }
}
