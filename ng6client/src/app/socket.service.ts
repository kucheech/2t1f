import { Injectable, OnInit } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable, Observer, Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private socket;

  constructor() {
    this.socket = io();
  }

  send(event, data) {
    this.socket.emit(event, data);
  }

  onEvent(event): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on(event, (data) => observer.next(data));
    });
  }

}
