import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SessionService } from '../session.service';
// import { Observable, Subject } from 'rxjs';
// import { map } from 'rxjs/operators';
import { SocketService } from '../socket.service';
import { Entry } from '../entry';
import { Title } from '@angular/platform-browser';
import { DataService } from '../data.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-add-entry',
  templateUrl: './add-entry.component.html',
  styleUrls: ['./add-entry.component.css']
})
export class AddEntryComponent implements OnInit {

  sid: string;
  entry: Entry = new Entry();
  addingEntry = true;
  name: string;

  constructor(
    private sessionService: SessionService,
    private socketService: SocketService,
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router,
    private dataService: DataService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.name = this.dataService.getName();
    if (this.name) {
      this.setTitle('Welcome, ' + this.name);
      this.sid = this.dataService.getSessionId();
      this.addingEntry = false;
    } else {
      this.route.params.subscribe((params: Params) => {
        this.sid = params['sid'];
        this.dataService.setSessionId(this.sid);
      });

      this.setTitle('Welcome');
      this.addingEntry = true;
    }

    this.socketService.onEvent(this.sid)
      .subscribe(msg => this.processMsg(msg));

    this.spinner.hide();
  }

  processMsg(msg) {
    if (msg && msg.command === 'start-game') {
      this.spinner.show();
      this.router.navigateByUrl('/guess');
    }
  }

  submitEntry() {
    this.entry.sid = this.sid;
    this.sessionService.submitEntry(this.entry);
    this.name = this.entry.name;
    this.dataService.setName(this.name);
    this.setTitle('Welcome, ' + this.name);
    this.addingEntry = false;
  }

  setTitle(msg) {
    this.titleService.setTitle(msg);
  }
}
