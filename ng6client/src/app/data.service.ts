import { Injectable } from '@angular/core';
import { Entry } from './entry';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  sid: string = null;
  entries: Entry[] = [];
  name: string = null;
  statements: string[] = [];
  voted = -1;
  previousVote = -1;
  showAnswer = false;
  score = 0;
  answer = -1;

  constructor() {
    if (sessionStorage.sid) {
      this.sid = sessionStorage.sid;
    }
    if (sessionStorage.entries) {
      this.entries = JSON.parse(sessionStorage.entries);
    }
    if (sessionStorage.name) {
      this.name = sessionStorage.name;
    }
    if (sessionStorage.statements) {
      this.statements = JSON.parse(sessionStorage.statements);
    }
    if (sessionStorage.voted) {
      this.voted = parseInt(sessionStorage.voted, 10);
    }
    if (sessionStorage.previousVote) {
      this.previousVote = parseInt(sessionStorage.previousVote, 10);
    }
    if (sessionStorage.showAnswer) {
      this.showAnswer = true;
    }
    if (sessionStorage.score) {
      this.score = parseInt(sessionStorage.score, 10);
    }
    if (sessionStorage.answer) {
      this.answer = parseInt(sessionStorage.answer, 10);
    }
  }

  getSessionId() {
    return this.sid;
  }

  setSessionId(sid) {
    this.sid = sid;
    sessionStorage.setItem('sid', sid);
  }

  getEntries() {
    return this.entries;
  }

  setEntries(entries) {
    this.entries = entries;
    sessionStorage.setItem('entries', JSON.stringify(entries));
  }

  getName() {
    return this.name;
  }

  setName(name) {
    this.name = name;
    sessionStorage.setItem('name', name);
  }

  getStatements() {
    return this.statements;
  }

  setStatements(statements) {
    this.statements = statements;
    sessionStorage.setItem('statements', JSON.stringify(statements));
  }

  getVoted() {
    return this.voted;
  }

  setVoted(v) {
    this.voted = v;
    sessionStorage.setItem('voted', String(v));
  }

  setPreviousVote(v) {
    this.previousVote = v;
    sessionStorage.setItem('previousVote', String(v));
  }

  getShowAnswer() {
    return this.showAnswer;
  }

  setShowAnswer(status) {
    this.showAnswer = status;
    if (status) {
      sessionStorage.setItem('showAnswer', 'true');
    } else {
      sessionStorage.removeItem('showAnswer');
    }
  }

  setAnswer(i) {
    this.answer = i;
    sessionStorage.setItem('answer', String(i));
  }

  setScore(i) {
    this.score = i;
    sessionStorage.setItem('score', String(i));
  }

  getGuessEntryState() {
    return {
      sid: this.sid,
      name: this.name,
      statements: this.statements,
      voted: this.voted,
      previousVote: this.previousVote,
      showAnswer: this.showAnswer,
      score: this.score,
      answer: this.answer
    };
  }

  clearData() {
    this.sid = null;
    this.entries = null;
    this.statements = null;
    this.voted = null;
    this.previousVote = null;
    this.showAnswer = null;
    this.score = null;
    this.answer = null;
    sessionStorage.clear();
  }
}
