import { Component, OnInit } from '@angular/core';
import { Entry } from '../entry';
import { DataService } from '../data.service';
import { Title } from '@angular/platform-browser';
import { SessionService } from '../session.service';
import { SocketService } from '../socket.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-show-entry',
  templateUrl: './show-entry.component.html',
  styleUrls: ['./show-entry.component.css']
})
export class ShowEntryComponent implements OnInit {

  entry: Entry = null;
  entries: Entry[];
  votes: number[] = [0, 0, 0];
  isAnswer: boolean[];
  showAnswer = false;
  statements: string[];
  bootstrapTypes = this.shuffle(['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark']);
  titlePrefixes = ['wrt', '3 things about', 'Do you really know', 'It\'s about', 'Something weird about', 'Hey, it\'s',
    'How well do you know', 'Once upon a time..', 'What\'s the fake thing about', 'Don\'t cha u wish u noe bout'];
  turn = 0;
  sid: string;
  showVotes = false;

  constructor(private dataService: DataService,
    private titleService: Title,
    private sessionService: SessionService,
    private socketService: SocketService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.entries = this.dataService.getEntries();

    if (this.entries.length) {
      this.sid = this.entries[0].sid;
      this.sessionService.startGame(this.sid).subscribe(() => {
        this.showEntry(0);
        this.spinner.hide();
      });

      this.socketService.onEvent(this.sid)
        .subscribe(msg => this.processMsg(msg));
    }

  }

  processMsg(msg) {
    if (msg.command && msg.command === 'vote') {
      this.updateVotes(msg.vote, msg.previousVote);
    }
  }

  updateVotes(v, pv) {
    this.votes[v]++;
    if (pv !== -1) {
      this.votes[pv]--;
    }
  }

  reveal() {
    this.showAnswer = true;
    this.sendAnswer();
  }

  setEntry(entry) {
    this.entry = entry;
    this.titleService.setTitle(this.getRandomTitlePrefix() + ' ' + entry.name);
  }

  getRandomTitlePrefix() {
    return this.titlePrefixes[Math.floor(Math.random() * this.titlePrefixes.length)];
  }

  sendAnswer() {
    this.socketService.send('relay', { sid: this.sid, command: 'show-answer', answer: this.getAnswer() });
  }

  sendStatements() {
    this.socketService.send('relay', { sid: this.sid, command: 'display-statements', name: this.entry.name, statements: this.statements });
  }

  setStatements() {
    this.statements = this.shuffle([this.entry.t1, this.entry.t2, this.entry.f]);
  }

  getAnswer() {
    return this.isAnswer.indexOf(true);
  }

  setAnswer() {
    this.isAnswer = this.statements.map((s) => (s === this.entry.f));
  }

  // https://stackoverflow.com/a/12646864
  shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }

    return array;
  }

  getBadgeClass(i) {
    return 'badge badge-pill badge-' + this.bootstrapTypes[i];
  }

  showEntry(i) {
    this.turn = i;
    this.setEntry(this.entries[i]);
    this.setStatements();
    this.setAnswer();
    this.reset();

    this.sendStatements();
  }

  reset() {
    this.showAnswer = false;
    this.votes = [0, 0, 0];
  }

}
