import { Component, OnInit } from '@angular/core';
import { SocketService } from '../socket.service';
import { DataService } from '../data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-guess-entry',
  templateUrl: './guess-entry.component.html',
  styleUrls: ['./guess-entry.component.css']
})
export class GuessEntryComponent implements OnInit {

  name: string = null;
  statements: string[] = null;
  sid: string = null;
  voted = -1;
  previousVote = -1;
  score = 0;
  showAnswer = false;
  answer = -1;

  constructor(private dataService: DataService,
    private titleService: Title,
    private socketService: SocketService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    const guessEntryState = this.dataService.getGuessEntryState();
    this.sid = guessEntryState.sid;
    if (guessEntryState.statements) { // loading a previous state
      this.displayStatements(guessEntryState.name, guessEntryState.statements);
      this.voted = guessEntryState.voted;
      this.previousVote = guessEntryState.previousVote;
      this.score = guessEntryState.score;
      this.showAnswer = guessEntryState.showAnswer;
      this.answer = guessEntryState.answer;
    }
    this.startSocket();
    this.spinner.hide();
  }

  startSocket() {
    this.socketService.onEvent(this.sid)
      .subscribe(msg => this.processMsg(msg));
  }

  processMsg(msg) {
    switch (msg.command) {
      case 'display-statements':
        this.spinner.show();
        this.displayStatements(msg.name, msg.statements);
        this.cacheGuessEntryStateBeforeShowAnswer();
        this.spinner.hide();
        break;

      case 'show-answer':
        this.updateView(msg.answer);
        this.cacheGuessEntryStateAfterShowAnswer();
        break;
    }
  }

  updateView(answer) {
    this.answer = answer;
    if (this.voted === answer) {
      this.score++;
    }
    this.showAnswer = true;
  }

  displayStatements(name, statements) {
    this.name = name;
    this.statements = statements;
    this.voted = -1;
    this.previousVote = -1;
    this.showAnswer = false;
    this.answer = -1;
    this.titleService.setTitle('Guess for ' + this.name);
  }

  cacheGuessEntryStateBeforeShowAnswer() {
    this.dataService.setName(this.name);
    this.dataService.setStatements(this.statements);
    this.dataService.setVoted(-1);
    this.dataService.setShowAnswer(false);
    this.dataService.setAnswer(-1);
  }

  cacheGuessEntryStateAfterShowAnswer() {
    this.dataService.setShowAnswer(true);
    this.dataService.setAnswer(this.answer);
    this.dataService.setScore(this.score);
  }

  vote(i) {
    if (this.showAnswer) {
      return; // ignore if host had already revealed answer
    }

    this.voted = i;
    this.sendVote(i);
    this.dataService.setVoted(i);
  }

  sendVote(i) {
    if (i !== this.previousVote) {
      this.socketService.send('relay', { sid: this.sid, command: 'vote', vote: i, previousVote: this.previousVote });
      this.previousVote = i;
      this.dataService.setPreviousVote(i);
    }
  }
}
