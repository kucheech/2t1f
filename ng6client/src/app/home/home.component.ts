import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  sessionId: string;

  constructor(private titleService: Title,
    private dataService: DataService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.titleService.setTitle('2T1F');
    this.dataService.clearData();
    this.spinner.hide();
  }

  isJoinDisabled() {
    return this.sessionId && this.sessionId.length !== 4;
  }

}
