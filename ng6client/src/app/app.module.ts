import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { SessionComponent } from './session/session.component';
import { HomeComponent } from './home/home.component';
import { AddEntryComponent } from './add-entry/add-entry.component';
import { QRCodeComponent } from './qrcode/qrcode.component';
import { ShowEntryComponent } from './show-entry/show-entry.component';
import { GuessEntryComponent } from './guess-entry/guess-entry.component';

import { UiSwitchModule } from 'ngx-ui-switch';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    AppComponent,
    SessionComponent,
    HomeComponent,
    AddEntryComponent,
    QRCodeComponent,
    ShowEntryComponent,
    GuessEntryComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    UiSwitchModule,
    NgxSpinnerModule
  ],
  providers: [
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
