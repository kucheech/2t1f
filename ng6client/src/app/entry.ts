export class Entry {
  constructor(
    public sid: string = null,
    public name: string = null,
    public t1: string = null,
    public t2: string = null,
    public f: string = null) { }
}
