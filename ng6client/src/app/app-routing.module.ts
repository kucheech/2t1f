import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { SessionComponent } from './session/session.component';
import { AddEntryComponent } from './add-entry/add-entry.component';
import { ShowEntryComponent } from './show-entry/show-entry.component';
import { GuessEntryComponent } from './guess-entry/guess-entry.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'session', component: SessionComponent },
  { path: 'entry/add/:sid', component: AddEntryComponent },
  { path: 'entries', component: ShowEntryComponent },
  { path: 'guess', component: GuessEntryComponent }
];

@NgModule({
  imports: [ RouterModule, RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
